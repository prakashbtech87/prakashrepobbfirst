package com.sortkp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class TestQuickSort {

	@Autowired
	QuickSort quickSort;

	@Test
	public void testQuickSort() {
		quickSort.doQuickSort();
	}

	@Test
	public void testMain() {
		quickSort.main(null);
	}

}
