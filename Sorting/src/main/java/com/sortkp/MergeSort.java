package com.sortkp;

import java.util.Arrays;

public class MergeSort {

	public static void main(String[] args) {
		doMergeSort();
	}

	public static void doMergeSort() {
		Integer[] A = new Integer[] { 7, 6, 10, 5, 9, 2, 1, 15, 62 };
		int lb = 0;
		int ub = A.length - 1;
		System.out.println("Before Sort :" + Arrays.toString(A));
		mergeSort(A, lb, ub);
		System.out.println("After Sort  :" + Arrays.toString(A));
	}

	private static void mergeSort(Integer[] A, int lb, int ub) {
		if (lb < ub) {
			int mid = (ub + lb) / 2;
			mergeSort(A, lb, mid);
			mergeSort(A, mid + 1, ub);
			merge(A, lb, mid, ub);
		}

	}

	private static void merge(Integer[] a, int lb, int mid, int ub) {

		int b[] = new int[a.length];
		int i = lb;
		int j = mid + 1;
		int k = lb;
		while (i <= mid && j <= ub) {

			if (a[i] <= a[j]) {
				b[k] = a[i];
				i++;
				k++;
			} else {
				b[k] = a[j];
				j++;
				k++;
			}

		}

		if (i > mid) {
			while (j <= ub) {
				b[k] = a[j];
				j++;
				k++;
			}
		} else {
			while (i <= mid) {
				b[k] = a[i];
				i++;
				k++;
			}
		}
		for (k = lb; k <= ub; k++) {
			a[k] = b[k];
		}
	}
}
