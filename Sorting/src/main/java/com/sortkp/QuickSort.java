package com.sortkp;

import java.util.Arrays;

public class QuickSort {

	public static void main(String[] args) {
		doQuickSort();
	}

	public static void doQuickSort() {
		Integer[] A = new Integer[] { 7, 6, 10, 5, 9, 2, 1, 15, 62 };
		int lb = 0;
		int ub = A.length - 1;
		System.out.println("Before Sort :" + Arrays.toString(A));
		quickSort(A, lb, ub);
		System.out.println("After Sort  :" + Arrays.toString(A));
	}

	private static void quickSort(Integer[] A, int lb, int ub) {

		if (lb < ub) {
			int loc = partition(A, lb, ub);
			quickSort(A, lb, loc - 1);
			quickSort(A, loc + 1, ub);
		}

	}

	private static int partition(Integer[] A, int lb, int ub) {

		int pivot = A[lb];
		int start = lb;
		int end = ub;

		while (start < end) {
			while (A[start] <= pivot) {
				start++;
			}
			while (A[end] > pivot) {
				end--;
			}
			if (start < end) {
				Integer temp = A[start];
				A[start] = A[end];
				A[end] = temp;
			}
		}
		Integer temp = A[lb];
		A[lb] = A[end];
		A[end] = temp;
		return end;
	}

}
